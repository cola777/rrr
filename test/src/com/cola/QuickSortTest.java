package com.cola;

public class QuickSortTest {

    public static void main(String[] args) {

        int[] a = {1,33,5,2,4,556,2};

        quickSort(a, 0, a.length-1);

        for (int i : a) {

            System.out.print(i + " ");
        }
    }

    public static void quickSort(int[] a, int left, int rigth){

        if (left >= rigth) return;

        int i = left, j = rigth, key = a[i];

        while (i<j) {

            while (i<j && a[j] >= key) j--;

            a[i] = a[j];

            while (i<j && a[i] <=key) i++;

            a[j] = a[i];
        }

        a[i] = key;

        quickSort(a, left, i -1);
        quickSort(a, i+1, rigth);


    }
}
